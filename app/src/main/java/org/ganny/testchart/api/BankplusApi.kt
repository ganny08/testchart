package org.ganny.testchart.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.ganny.testchart.model.PointListResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

interface BankPlusApi {
    // demo.bankplus.ru/mobws/json/pointList
    @FormUrlEncoded
    @POST("mobws/json/pointList")
    fun pointListAsync(@Field("count") count: Int,
                       @Field("version") version: String = VERSION): Deferred<Response<PointListResponse>>

    companion object Factory {
        private const val HOST = "demo.bankplus.ru"
        private const val REQUEST_TIMEOUT = 10000L
        const val VERSION = "1.1"

        val instance: BankPlusApi by lazy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.MILLISECONDS)
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.MILLISECONDS)
                .addInterceptor(logging)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl("https://$HOST/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .callbackExecutor(Executors.newSingleThreadExecutor())
                .build()
            retrofit.create(BankPlusApi::class.java)
        }
    }
}