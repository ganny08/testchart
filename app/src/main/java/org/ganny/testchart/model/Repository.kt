package org.ganny.testchart.model

import org.ganny.testchart.api.BankPlusApi

class Repository {
    suspend fun getPointList(count: Int): PointList? {
        val result = BankPlusApi.instance.pointListAsync(count).await()
        if(result.isSuccessful) {
            when(result.body()!!.result) {
                0 -> return result.body()?.response
                // TODO -1, -100 and other
            }
        }
        return null // TODO подумать как обрабатывать ошибку
    }
}