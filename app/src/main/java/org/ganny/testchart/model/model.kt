package org.ganny.testchart.model

data class Response<T>(
    val response: T
)

data class PointListResponse(
    val result: Int,
    val response: PointList?,
    val message: String?
)

data class PointList(
    val points: Array<Point>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PointList

        if (!points.contentEquals(other.points)) return false

        return true
    }

    override fun hashCode(): Int {
        return points.contentHashCode()
    }
}

data class Point(
    val x: Double,
    val y: Double
)