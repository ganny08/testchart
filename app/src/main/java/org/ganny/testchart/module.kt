package org.ganny.testchart

import org.ganny.testchart.model.Repository

fun module() = org.koin.dsl.module {
    single { Repository() }
}