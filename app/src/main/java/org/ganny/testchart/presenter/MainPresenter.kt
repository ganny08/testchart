package org.ganny.testchart.presenter


import kotlinx.coroutines.*
import moxy.InjectViewState
import moxy.MvpPresenter
import org.ganny.testchart.model.Repository
import org.ganny.testchart.toLog
import org.koin.core.KoinComponent
import org.koin.core.inject

@InjectViewState
class MainPresenter: MvpPresenter<MainView>(), KoinComponent {

    val repository: Repository by inject()

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.IO + job)

    fun runOnClick(pointCount: Int) {
        scope.launch {
            try {
                val list = repository.getPointList(pointCount)
                if (list != null)
                    viewState.openChart()
            } catch (ex: Exception) {
                ex.toLog()
                // TODO сделат вывод инфосообщения
            }
        }
    }
}