package org.ganny.testchart.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_main.*
import moxy.MvpAppCompatFragment
import moxy.MvpFragment
import moxy.presenter.InjectPresenter

import org.ganny.testchart.R
import org.ganny.testchart.presenter.MainPresenter
import org.ganny.testchart.presenter.MainView
import org.ganny.testchart.toLog
import java.lang.NumberFormatException

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : MvpAppCompatFragment(), MainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        main_btn_run.setOnClickListener {
            val count = try {
                main_et_count.text.toString().toInt()
            } catch (ex: NumberFormatException) {
                ex.toLog()
                Toast.makeText(context!!, R.string.input_error, Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            presenter.runOnClick(count)
        }
    }

    override fun openChart() {
        findNavController().navigate(R.id.action_mainFragment_to_chartFragment)
    }
}
